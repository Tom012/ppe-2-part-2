﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.Odbc; //Objets d'accès à une base de données Access

namespace _PPE2__MORINMOREL
{
    public partial class Fonctionnalite1 : Form
    {
        // Variables globales au formulaire

        OdbcConnection cnn = new OdbcConnection();
        OdbcCommand cmd = new OdbcCommand();
        OdbcDataReader drr;
        Boolean existenreg;

        public Fonctionnalite1()
        {
            InitializeComponent();
        }

        private void Fonctionnalite1_Load_1(object sender, EventArgs e)
        {
            // Accès à la base de données
            cnn.ConnectionString = "Driver={MySQL ODBC 5.2 ANSI Driver};SERVER=localhost;Database=camping;uid=root;pwd=";
            cnn.Open();
            cmd.CommandText = "select * from typemobil";
            cmd.Connection = cnn;
            drr = cmd.ExecuteReader();
            existenreg = drr.Read();
            // Boucle d'affichage des données
            while (existenreg == true)
            {
                grdAffType.Rows.Add(drr["idtyp"], drr["libtyp"], drr["nbpers"], drr["descripcourte"], drr["descriplongue"], drr["tarifsemaine"]);
                existenreg = drr.Read();
            }
            drr.Close();
            cnn.Close();
        }

        private void btnAjout_Click(object sender, EventArgs e)
        {
            grdAffType.Rows.Clear();
            // Accès à la base de données
            cnn.ConnectionString = "Driver={MySQL ODBC 5.2 ANSI Driver};SERVER=localhost;Database=camping;uid=root;pwd=";
            cnn.Open();
            cmd.CommandText = "insert into typemobil(libtyp, nbpers, descripcourte, descriplongue, tarifsemaine) value('" + txtNom.Text + "','" + txtNbPers.Text + "','" + txtDescCourt.Text + "','" + txtDescLong.Text + "','" + txtPrix.Text + "');";
            cmd.Connection = cnn;
            cmd.ExecuteNonQuery();
            cnn.Close();
            // Boucle d'affichage des données
            Fonctionnalite1_Load_1(sender, e);
        }
        private void btnSuppr_Click(object sender, EventArgs e)
        {
            grdAffType.Rows.Clear();
            // Accès à la base de données
            cnn.ConnectionString = "Driver={MySQL ODBC 5.2 ANSI Driver};SERVER=localhost;Database=camping;uid=root;pwd=";
            cnn.Open();
            cmd.CommandText = "delete from typemobil where idtyp ='" + txtID.Text + "';";
            cmd.Connection = cnn;
            cmd.ExecuteNonQuery();
            cnn.Close();
            // Boucle d'affichage des données
            Fonctionnalite1_Load_1(sender, e);
        }

        private void btnModif_Click(object sender, EventArgs e)
        {
            grdAffType.Rows.Clear();
            // Accès à la base de données
            cnn.ConnectionString = "Driver={MySQL ODBC 5.2 ANSI Driver};SERVER=localhost;Database=camping;uid=root;pwd=";
            cnn.Open();
            cmd.CommandText = "update typemobil set libtyp ='" + txtNom.Text + "', nbpers ='" + txtNbPers.Text + "', descripcourte ='" + txtDescCourt.Text + "', descriplongue ='" + txtDescLong.Text + "', tarifsemaine ='" + txtPrix.Text + "' where idtyp='" + txtID.Text + "';";
            cmd.Connection = cnn;
            cmd.ExecuteNonQuery();
            cnn.Close();
            // Boucle d'affichage des données
            Fonctionnalite1_Load_1(sender, e);
        }
    }
}