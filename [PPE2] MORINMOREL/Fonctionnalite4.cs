﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.Odbc;

namespace _PPE2__MORINMOREL
{
    public partial class Fonctionnalite4 : Form
    {
        // Variable pour convertir des dates
        DateTime datedebut;
        DateTime datefin;
        DateTime dateres;

        // Chaine de connection
        OdbcConnection chemin = new OdbcConnection();

        //Requete SQL
        OdbcCommand requete = new OdbcCommand();

        //Variable contenant les enregistrements lu
        OdbcDataReader enregistrements;

        //Variable permettant de savoir si la ligne suivant existe
        Boolean presence;

        public Fonctionnalite4()
        {
            InitializeComponent();
        }

        private void Fonctionnalite4_Load(object sender, EventArgs e)
        {
            lblDate.Text = "";
            lstres.Items.Clear();
            txtAdresse.Text="";
            txtCp.Text = "";
            txtIdcli.Text = "";
            txtMail.Text = "";
            txtNom.Text = "";
            txtNomcli.Text = "";
            txtNum.Text = "";
            txtTel.Text = ""; ;
            txtVille.Text = "";
        }

        private void btnNum_Click(object sender, EventArgs e)
        {
            lstres.Items.Clear();
            // Connexion à la base de données
            chemin.ConnectionString = "Driver={MySQL ODBC 5.2 ANSI Driver};SERVER=localhost;Database=camping;uid=root;pwd=";
            chemin.Open();

            // Requête SQL
            requete.CommandText = "SELECT * FROM reservation INNER JOIN client ON reservation.idcli=client.idcli WHERE idres='" + txtNum.Text + "';";
            requete.Connection = chemin;
            enregistrements = requete.ExecuteReader();
            presence = enregistrements.Read();

            // Affichage du résultatde la requête dans la liste
            while (presence == true)
            {
                datedebut=Convert.ToDateTime(enregistrements["datedebut"]);
                lstres.Items.Add(enregistrements["nomcli"]);
                presence = enregistrements.Read();
            }
            enregistrements.Close();
            chemin.Close();
        }

        private void btnNom_Click(object sender, EventArgs e)
        {
            lstres.Items.Clear();
            // Connexion à la base de données
            chemin.ConnectionString = "Driver={MySQL ODBC 5.2 ANSI Driver};SERVER=localhost;Database=camping;uid=root;pwd=";
            chemin.Open();

            // Requête SQL
            requete.CommandText = requete.CommandText = "SELECT * FROM reservation INNER JOIN client ON reservation.idcli=client.idcli WHERE nomcli LIKE('" + txtNom.Text + "%');";
            requete.Connection = chemin;
            enregistrements = requete.ExecuteReader();
            presence = enregistrements.Read();

            // Affichage du résultatde la requête dans la liste
            while (presence == true)
            {
                datedebut = Convert.ToDateTime(enregistrements["datedebut"]);
                lstres.Items.Add(enregistrements["nomcli"]);
                presence = enregistrements.Read();
            }
            enregistrements.Close();
            chemin.Close();
        }

        private void lstres_Click(object sender, EventArgs e)
        {
            chemin.ConnectionString = "Driver={MySQL ODBC 5.2 ANSI Driver};SERVER=localhost;Database=camping;uid=root;pwd=";
            chemin.Open();

            // Requête SQL
            requete.CommandText = "SELECT * FROM client INNER JOIN reservation ON reservation.idcli=client.idcli WHERE nomcli LIKE('" + lstres.SelectedItem + "');";
            requete.Connection = chemin;
            enregistrements = requete.ExecuteReader();
            presence = enregistrements.Read();

            while (presence == true)
            {
                // Assignation des valeurs obtenu lors de la requête
                txtNomcli.Text = Convert.ToString((enregistrements["nomcli"]));
                txtAdresse.Text = Convert.ToString((enregistrements["adresse"]));
                txtCp.Text = Convert.ToString((enregistrements["cp"]));
                txtVille.Text = Convert.ToString((enregistrements["ville"]));
                txtTel.Text = Convert.ToString((enregistrements["telephone"]));
                txtMail.Text = Convert.ToString((enregistrements["mail"]));
                txtIdcli.Text = Convert.ToString((enregistrements["idcli"]));
                dateres = Convert.ToDateTime((enregistrements["dateres"]));
                datedebut = Convert.ToDateTime((enregistrements["datedebut"]));
                datefin = Convert.ToDateTime((enregistrements["datefin"]));

                // Convertion des dates récupéré au bon format
                lblDate.Text = "Date de réservation : " + dateres.ToString("dd/MM/yyyy") + "\nDate de début : " + datedebut.ToString("dd/MM/yyyy") + "\nDate de fin : " + datefin.ToString("dd/MM/yyyy");

                if (Convert.ToInt16(enregistrements["regleon"]) == 1)
                {
                    rdPaye.Checked = true;
                    rdNoPaye.Checked = false;
                }
                else
                {
                    rdNoPaye.Checked = true;
                    rdPaye.Checked = false;
                }

                presence = enregistrements.Read();
            }

            txtNum.Text = "";
            txtNom.Text = "";
            lstres.Items.Clear();

            enregistrements.Close();
            chemin.Close();
        }

        private void btnModif_Click(object sender, EventArgs e)
        {
            // Mise à jour de réservation
            int reglement;
            if (rdPaye.Checked == true)
            {
                reglement = 1;
            }
            else
            {
                reglement = 0;
            }

            // Modification de la réservation
            chemin.ConnectionString = "Driver={MySQL ODBC 5.2 ANSI Driver};SERVER=localhost;Database=camping;uid=root;pwd=";
            chemin.Open();
            requete.CommandText = "UPDATE reservation SET regleon=" + reglement + " WHERE idcli=" + Convert.ToInt16(txtIdcli.Text);
            requete.Connection = chemin;
            enregistrements = requete.ExecuteReader();
            presence = enregistrements.Read();
            while (presence == true)
            {
                presence = enregistrements.Read();
            }
            enregistrements.Close();
            chemin.Close();

            // Mise à jour du client
            chemin.ConnectionString = "Driver={MySQL ODBC 5.2 ANSI Driver};SERVER=localhost;Database=camping;uid=root;pwd=";
            chemin.Open();
            requete.CommandText = "UPDATE client SET nomcli='" + txtNomcli.Text + "', adresse='" + txtAdresse.Text + "', cp='" + txtCp.Text + "', ville='" + txtVille.Text + "', telephone='" + txtTel.Text + "', mail='" + txtMail.Text + "' WHERE idcli=" + Convert.ToInt16(txtIdcli.Text);
            requete.Connection = chemin;
            enregistrements = requete.ExecuteReader();
            presence = enregistrements.Read();
            while (presence == true)
            {
                presence = enregistrements.Read();
            }
            enregistrements.Close();
            chemin.Close();

            // Reset des texxtBox et rdBtn
            txtNomcli.Text = "";
            txtAdresse.Text = "";
            txtCp.Text = "";
            txtVille.Text = "";
            txtTel.Text = "";
            txtMail.Text = "";
            txtIdcli.Text = "";
            lblDate.Text = "";
            rdNoPaye.Checked = false;
            rdPaye.Checked = false;
            lstres.Items.Clear();
        }

        private void btnExcel_Click(object sender, EventArgs e)
        {
            int idres =0;
            chemin.ConnectionString = "Driver={MySQL ODBC 5.2 ANSI Driver};SERVER=localhost;Database=camping;uid=root;pwd=";
            chemin.Open();
            requete.CommandText = "SELECT * FROM reservation WHERE idcli=" + Convert.ToInt16(txtIdcli.Text) + " ";
            requete.Connection = chemin;
            enregistrements = requete.ExecuteReader();
            presence = enregistrements.Read();
            while (presence == true)
            {
                
                idres = Convert.ToInt16(enregistrements["idres"]);
                presence = enregistrements.Read();
            }
            enregistrements.Close();

            //Edition Excel
            Excel.Workbook claexcel;
            Excel.Application appexcel;
            appexcel = new Excel.Application();
            appexcel.Visible = true;
            appexcel.Workbooks.Open(Application.StartupPath + "\\factureexcel.xls");

            requete.CommandText = "select * from client inner join reservation on client.idcli=reservation.idcli inner join mobilhome on reservation.idmob=mobilhome.idmob inner join typemobil on mobilhome.idtyp=typemobil.idtyp  where  idres='" + idres + "'";
            requete.Connection = chemin;
            enregistrements = requete.ExecuteReader();
            presence = enregistrements.Read();
            while (presence == true)
            {
                //autocomplétion des informations
                claexcel = appexcel.Workbooks.Item[1];
                appexcel.Sheets[1].cells[6, 2].value = enregistrements["idres"].ToString();
                appexcel.Sheets[1].cells[6, 7].value = enregistrements["nomcli"].ToString();
                appexcel.Sheets[1].cells[7, 7].value = enregistrements["adresse"].ToString();
                appexcel.Sheets[1].cells[8, 7].value = enregistrements["cp"].ToString() + "           ,";
                appexcel.Sheets[1].cells[8, 8].value = enregistrements["ville"].ToString();


                //Récupération de la date d'aujourd'hui
                appexcel.Sheets[1].cells[12, 7].value = DateTime.Today;
                //Recupération de la date de début et de fin
                appexcel.Sheets[1].cells[14, 3].value = Convert.ToDateTime(enregistrements["datedebut"]).ToString("dd/MM/yyyy");
                appexcel.Sheets[1].cells[14, 6].value = Convert.ToDateTime(enregistrements["datefin"]).ToString("dd/MM/yyyy");


                //Désignation
                appexcel.Sheets[1].cells[17, 2].value = enregistrements["nom"].ToString();

                //Calculs du nombre de jour de location
                DateTime deb = DateTime.Parse(enregistrements["datedebut"].ToString());
                DateTime fin = DateTime.Parse(enregistrements["datefin"].ToString());
                TimeSpan Diff = fin - deb;
                appexcel.Sheets[1].cells[17, 3].value = Diff.TotalDays;

                //Calcul du tarifs par jours
                appexcel.Sheets[1].cells[17, 4].value = (Convert.ToInt32(enregistrements["tarifsemaine"].ToString())) / 7;

                //Tarif HT
                appexcel.Sheets[1].cells[17, 5].value = "=SUM(C17*D17)";

                //Tarif TTC
                appexcel.Sheets[1].cells[17, 6].value = "=SUM(E17*(1+19.6/100))";

                claexcel = null;
                appexcel = null;
                presence = enregistrements.Read();

            }
            enregistrements.Close();
            chemin.Close();
        }
    }
}
