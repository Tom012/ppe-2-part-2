﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.Odbc; //Objets d'accès à une base de données Access

namespace _PPE2__MORINMOREL
{
    public partial class Fonctionnalite2 : Form
    {
        // Chaine de connection
        OdbcConnection chemin = new OdbcConnection();

        //Requete SQL
        OdbcCommand requete = new OdbcCommand();

        //Variable contenant les enregistrements lu
        OdbcDataReader enregistrements;

        //Variable permettant de savoir si la ligne suivant existe
        Boolean presence;

        public Fonctionnalite2()
        {
            InitializeComponent();
        }

        public void InittxtAdd()
        {
            txtNomAdd.Text = "";
            txtNumempAdd.Text = "";
        }

        private void Fonctionnalite2_Load(object sender, EventArgs e)
        {
            // Accès à la base de données
            chemin.ConnectionString = "Driver={MySQL ODBC 5.2 ANSI Driver};SERVER=localhost;Database=camping;uid=root;pwd=";
            chemin.Open();
            requete.CommandText = "Select * from Mobilhome join typemobil on mobilhome.idtyp=typemobil.idtyp;";
            requete.Connection = chemin;
            enregistrements = requete.ExecuteReader();
            presence = enregistrements.Read();
            // Boucle d'affichage des données
            while (presence == true)
            {
                grdMobilhome.Rows.Add(enregistrements["idmob"], enregistrements["nom"], enregistrements["numemp"], enregistrements["libtyp"]);
                presence = enregistrements.Read();
            }
            enregistrements.Close();
            requete.CommandText = "Select * from typemobil;";
            requete.Connection = chemin;
            enregistrements = requete.ExecuteReader();
            presence = enregistrements.Read();

            // Boucle d'affichage des données
            while (presence == true)
            {
                CboTypeAdd.Items.Add(enregistrements["libtyp"]);
                CboTypeMod.Items.Add(enregistrements["libtyp"]);
                presence = enregistrements.Read();
            }
            enregistrements.Close();
            chemin.Close();
        }

        private void btnAdd_Click_1(object sender, EventArgs e)
        {
            int idtyp = 0;
            chemin.ConnectionString = "Driver={MySQL ODBC 5.2 ANSI Driver};SERVER=localhost;Database=camping;uid=root;pwd=";
            chemin.Open();
            requete.CommandText = "SELECT idtyp FROM typemobil WHERE libtyp LIKE('" + CboTypeAdd.SelectedItem + "')";
            requete.Connection = chemin;
            enregistrements = requete.ExecuteReader();
            presence = enregistrements.Read();
            while (presence == true)
            {
                //Valeur du champ idtyp correspondant au libélé choisit.
                idtyp = (Convert.ToInt32(enregistrements["idtyp"]));
            presence = enregistrements.Read();
            }
            enregistrements.Close();
            chemin.Close();

            //Ajout dans la BD ainsi que ds le Grd
            chemin.ConnectionString = "Driver={MySQL ODBC 5.2 ANSI Driver};SERVER=localhost;Database=camping;uid=root;pwd=";
            chemin.Open();
            requete.CommandText = "INSERT INTO mobilhome (nom , numemp , idtyp) VALUES('" + txtNomAdd.Text + "' , " + txtNumempAdd.Text + " , " + idtyp + ")";
            requete.Connection = chemin;
            enregistrements = requete.ExecuteReader();
            presence = enregistrements.Read();
            while (presence == true)
            {
                grdMobilhome.Rows.Clear();
                grdMobilhome.Rows.Add(enregistrements["idmob"], enregistrements["nom"], enregistrements["numemp"], enregistrements["libtyp"]);
                presence = enregistrements.Read();
            }
            enregistrements.Close();
            chemin.Close();

            //Initialisation du grdMobilhome
            chemin.ConnectionString = "Driver={MySQL ODBC 5.2 ANSI Driver};SERVER=localhost;Database=camping;uid=root;pwd=";
            chemin.Open();
            requete.CommandText = "SELECT * FROM mobilhome inner join typemobil on mobilhome.idtyp=typemobil.idtyp ";
            requete.Connection = chemin;
            enregistrements = requete.ExecuteReader();
            presence = enregistrements.Read();
            while (presence == true)
            {
                grdMobilhome.Rows.Add(enregistrements["idmob"], enregistrements["nom"], enregistrements["numemp"], enregistrements["libtyp"]);
                presence = enregistrements.Read();
            }
            enregistrements.Close();
            chemin.Close();
            Fonctionnalite2_Load(sender, e);
        }

        private void btnModif_Click_1(object sender, EventArgs e)
        {
            //Valeur du champ idtyp correspondant au libélé choisit.
            int idtyp = 0;
            chemin.ConnectionString = "Driver={MySQL ODBC 5.2 ANSI Driver};SERVER=localhost;Database=camping;uid=root;pwd=";
            chemin.Open();
            requete.CommandText = "SELECT idtyp FROM typemobil WHERE libtyp LIKE('" + CboTypeMod.SelectedItem + "')";
            requete.Connection = chemin;
            enregistrements = requete.ExecuteReader();
            presence = enregistrements.Read();
            while (presence == true)
            {
                idtyp = (Convert.ToInt32(enregistrements["idtyp"]));
                presence = enregistrements.Read();
            }
            enregistrements.Close();
            chemin.Close();

            //Modif dans la BD ainsi que ds le Grd
            chemin.ConnectionString = "Driver={MySQL ODBC 5.2 ANSI Driver};SERVER=localhost;Database=camping;uid=root;pwd=";
            chemin.Open();
            requete.CommandText = "UPDATE mobilhome SET nom='" + txtNomMod.Text + "', numemp=" + txtNumempMod.Text + ", idtyp=" + idtyp + " WHERE idmob=" + txtIdMod.Text;
            requete.Connection = chemin;
            enregistrements = requete.ExecuteReader();
            presence = enregistrements.Read();
            while (presence == true)
            {
                presence = enregistrements.Read();
            }
            enregistrements.Close();
            chemin.Close();

            //Initialisation du grdMobilHome
            grdMobilhome.Rows.Clear();
            chemin.ConnectionString = "Driver={MySQL ODBC 5.2 ANSI Driver};SERVER=localhost;Database=camping;uid=root;pwd=";
            chemin.Open();
            requete.CommandText = "SELECT * FROM mobilhome inner join typemobil on mobilhome.idtyp=typemobil.idtyp ";
            requete.Connection = chemin;
            enregistrements = requete.ExecuteReader();
            presence = enregistrements.Read();
            while (presence == true)
            {
                grdMobilhome.Rows.Add(enregistrements["idmob"], enregistrements["nom"], enregistrements["numemp"], enregistrements["libtyp"]);
                presence = enregistrements.Read();
            }
            enregistrements.Close();
            chemin.Close();

            Fonctionnalite2_Load(sender, e);


        }

        private void btnDel_Click_1(object sender, EventArgs e)
        {
            grdMobilhome.Rows.Clear();
            // Accès à la base de données
            chemin.ConnectionString = "Driver={MySQL ODBC 5.2 ANSI Driver};SERVER=localhost;Database=camping;uid=root;pwd=";
            chemin.Open();
            requete.CommandText = "delete from mobilhome where idmob ='" + txtIdMod.Text + "';";
            requete.Connection = chemin;
            requete.ExecuteNonQuery();
            chemin.Close();
            // Boucle d'affichage des données
            Fonctionnalite2_Load(sender, e);
        }

        private void grdMobilhome_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int nolig = grdMobilhome.CurrentCell.RowIndex;
            txtIdMod.Text = grdMobilhome[0, nolig].Value.ToString();
            txtNomMod.Text = grdMobilhome[1, nolig].Value.ToString();
            txtNumempMod.Text = grdMobilhome[2, nolig].Value.ToString();
            CboTypeMod.Text = grdMobilhome[3, nolig].Value.ToString();
        }
    }
}
