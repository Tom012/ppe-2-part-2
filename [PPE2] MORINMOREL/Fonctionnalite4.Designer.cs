﻿namespace _PPE2__MORINMOREL
{
    partial class Fonctionnalite4
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtNum = new System.Windows.Forms.TextBox();
            this.txtNom = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnNom = new System.Windows.Forms.Button();
            this.btnNum = new System.Windows.Forms.Button();
            this.lstres = new System.Windows.Forms.ListBox();
            this.txtNomcli = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtIdcli = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtAdresse = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtCp = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtVille = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtTel = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtMail = new System.Windows.Forms.TextBox();
            this.lblDate = new System.Windows.Forms.Label();
            this.rdPaye = new System.Windows.Forms.RadioButton();
            this.rdNoPaye = new System.Windows.Forms.RadioButton();
            this.btnModif = new System.Windows.Forms.Button();
            this.btnExcel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtNum
            // 
            this.txtNum.Location = new System.Drawing.Point(414, 6);
            this.txtNum.Name = "txtNum";
            this.txtNum.Size = new System.Drawing.Size(75, 20);
            this.txtNum.TabIndex = 0;
            // 
            // txtNom
            // 
            this.txtNom.Location = new System.Drawing.Point(128, 6);
            this.txtNom.Name = "txtNom";
            this.txtNom.Size = new System.Drawing.Size(75, 20);
            this.txtNom.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(283, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(125, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Numéro de la réservation";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(110, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Nom de la réservation";
            // 
            // btnNom
            // 
            this.btnNom.Location = new System.Drawing.Point(128, 32);
            this.btnNom.Name = "btnNom";
            this.btnNom.Size = new System.Drawing.Size(75, 23);
            this.btnNom.TabIndex = 8;
            this.btnNom.Text = "Rechercher";
            this.btnNom.UseVisualStyleBackColor = true;
            this.btnNom.Click += new System.EventHandler(this.btnNom_Click);
            // 
            // btnNum
            // 
            this.btnNum.Location = new System.Drawing.Point(414, 32);
            this.btnNum.Name = "btnNum";
            this.btnNum.Size = new System.Drawing.Size(75, 23);
            this.btnNum.TabIndex = 9;
            this.btnNum.Text = "Rechercher";
            this.btnNum.UseVisualStyleBackColor = true;
            this.btnNum.Click += new System.EventHandler(this.btnNum_Click);
            // 
            // lstres
            // 
            this.lstres.FormattingEnabled = true;
            this.lstres.Location = new System.Drawing.Point(15, 85);
            this.lstres.Name = "lstres";
            this.lstres.Size = new System.Drawing.Size(188, 173);
            this.lstres.TabIndex = 10;
            this.lstres.Click += new System.EventHandler(this.lstres_Click);
            // 
            // txtNomcli
            // 
            this.txtNomcli.Location = new System.Drawing.Point(333, 85);
            this.txtNomcli.Name = "txtNomcli";
            this.txtNomcli.Size = new System.Drawing.Size(75, 20);
            this.txtNomcli.TabIndex = 11;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(256, 88);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 13);
            this.label3.TabIndex = 16;
            this.label3.Text = "Nom";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(256, 114);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(47, 13);
            this.label4.TabIndex = 18;
            this.label4.Text = "ID Client";
            // 
            // txtIdcli
            // 
            this.txtIdcli.Location = new System.Drawing.Point(333, 111);
            this.txtIdcli.Name = "txtIdcli";
            this.txtIdcli.Size = new System.Drawing.Size(75, 20);
            this.txtIdcli.TabIndex = 17;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(256, 140);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(45, 13);
            this.label5.TabIndex = 20;
            this.label5.Text = "Adresse";
            // 
            // txtAdresse
            // 
            this.txtAdresse.Location = new System.Drawing.Point(333, 137);
            this.txtAdresse.Name = "txtAdresse";
            this.txtAdresse.Size = new System.Drawing.Size(156, 20);
            this.txtAdresse.TabIndex = 19;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(256, 166);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(64, 13);
            this.label6.TabIndex = 22;
            this.label6.Text = "Code Postal";
            // 
            // txtCp
            // 
            this.txtCp.Location = new System.Drawing.Point(333, 163);
            this.txtCp.Name = "txtCp";
            this.txtCp.Size = new System.Drawing.Size(75, 20);
            this.txtCp.TabIndex = 21;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(256, 189);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(26, 13);
            this.label7.TabIndex = 24;
            this.label7.Text = "Ville";
            // 
            // txtVille
            // 
            this.txtVille.Location = new System.Drawing.Point(333, 186);
            this.txtVille.Name = "txtVille";
            this.txtVille.Size = new System.Drawing.Size(75, 20);
            this.txtVille.TabIndex = 23;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(256, 215);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(64, 13);
            this.label8.TabIndex = 26;
            this.label8.Text = "Télépohone";
            // 
            // txtTel
            // 
            this.txtTel.Location = new System.Drawing.Point(333, 212);
            this.txtTel.Name = "txtTel";
            this.txtTel.Size = new System.Drawing.Size(156, 20);
            this.txtTel.TabIndex = 25;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(256, 241);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(26, 13);
            this.label9.TabIndex = 28;
            this.label9.Text = "Mail";
            // 
            // txtMail
            // 
            this.txtMail.Location = new System.Drawing.Point(333, 238);
            this.txtMail.Name = "txtMail";
            this.txtMail.Size = new System.Drawing.Size(156, 20);
            this.txtMail.TabIndex = 27;
            // 
            // lblDate
            // 
            this.lblDate.AutoSize = true;
            this.lblDate.Location = new System.Drawing.Point(414, 85);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(41, 13);
            this.lblDate.TabIndex = 29;
            this.lblDate.Text = "label10";
            // 
            // rdPaye
            // 
            this.rdPaye.AutoSize = true;
            this.rdPaye.Location = new System.Drawing.Point(259, 274);
            this.rdPaye.Name = "rdPaye";
            this.rdPaye.Size = new System.Drawing.Size(49, 17);
            this.rdPaye.TabIndex = 30;
            this.rdPaye.TabStop = true;
            this.rdPaye.Text = "Payé";
            this.rdPaye.UseVisualStyleBackColor = true;
            // 
            // rdNoPaye
            // 
            this.rdNoPaye.AutoSize = true;
            this.rdNoPaye.Location = new System.Drawing.Point(259, 298);
            this.rdNoPaye.Name = "rdNoPaye";
            this.rdNoPaye.Size = new System.Drawing.Size(71, 17);
            this.rdNoPaye.TabIndex = 31;
            this.rdNoPaye.TabStop = true;
            this.rdNoPaye.Text = "Non payé";
            this.rdNoPaye.UseVisualStyleBackColor = true;
            // 
            // btnModif
            // 
            this.btnModif.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnModif.Location = new System.Drawing.Point(15, 274);
            this.btnModif.Name = "btnModif";
            this.btnModif.Size = new System.Drawing.Size(188, 41);
            this.btnModif.TabIndex = 32;
            this.btnModif.Text = "APPLIQUER MODIFICATION";
            this.btnModif.UseVisualStyleBackColor = true;
            this.btnModif.Click += new System.EventHandler(this.btnModif_Click);
            // 
            // btnExcel
            // 
            this.btnExcel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExcel.Location = new System.Drawing.Point(349, 274);
            this.btnExcel.Name = "btnExcel";
            this.btnExcel.Size = new System.Drawing.Size(140, 41);
            this.btnExcel.TabIndex = 33;
            this.btnExcel.Text = "IMPRESSION EXCEL";
            this.btnExcel.UseVisualStyleBackColor = true;
            this.btnExcel.Click += new System.EventHandler(this.btnExcel_Click);
            // 
            // Fonctionnalite4
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(766, 422);
            this.Controls.Add(this.btnExcel);
            this.Controls.Add(this.btnModif);
            this.Controls.Add(this.rdNoPaye);
            this.Controls.Add(this.rdPaye);
            this.Controls.Add(this.lblDate);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txtMail);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txtTel);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtVille);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtCp);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtAdresse);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtIdcli);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtNomcli);
            this.Controls.Add(this.lstres);
            this.Controls.Add(this.btnNum);
            this.Controls.Add(this.btnNom);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtNom);
            this.Controls.Add(this.txtNum);
            this.Name = "Fonctionnalite4";
            this.Text = "Fonctionnalite4";
            this.Load += new System.EventHandler(this.Fonctionnalite4_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtNum;
        private System.Windows.Forms.TextBox txtNom;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnNom;
        private System.Windows.Forms.Button btnNum;
        private System.Windows.Forms.ListBox lstres;
        private System.Windows.Forms.TextBox txtNomcli;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtIdcli;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtAdresse;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtCp;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtTel;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtMail;
        private System.Windows.Forms.TextBox txtVille;
        private System.Windows.Forms.Label lblDate;
        private System.Windows.Forms.RadioButton rdPaye;
        private System.Windows.Forms.RadioButton rdNoPaye;
        private System.Windows.Forms.Button btnModif;
        private System.Windows.Forms.Button btnExcel;
    }
}