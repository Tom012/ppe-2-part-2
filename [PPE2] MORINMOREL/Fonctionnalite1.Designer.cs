﻿namespace _PPE2__MORINMOREL
{
    partial class Fonctionnalite1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grdAffType = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnAjout = new System.Windows.Forms.Button();
            this.btnModif = new System.Windows.Forms.Button();
            this.btnSuppr = new System.Windows.Forms.Button();
            this.txtNom = new System.Windows.Forms.TextBox();
            this.txtPrix = new System.Windows.Forms.TextBox();
            this.txtDescLong = new System.Windows.Forms.TextBox();
            this.txtDescCourt = new System.Windows.Forms.TextBox();
            this.txtNbPers = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtID = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.grdAffType)).BeginInit();
            this.SuspendLayout();
            // 
            // grdAffType
            // 
            this.grdAffType.BackgroundColor = System.Drawing.Color.LightGray;
            this.grdAffType.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdAffType.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4,
            this.Column5,
            this.Column6});
            this.grdAffType.Location = new System.Drawing.Point(254, 10);
            this.grdAffType.Margin = new System.Windows.Forms.Padding(2);
            this.grdAffType.Name = "grdAffType";
            this.grdAffType.RowTemplate.Height = 24;
            this.grdAffType.Size = new System.Drawing.Size(889, 436);
            this.grdAffType.TabIndex = 9;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "ID";
            this.Column1.Name = "Column1";
            this.Column1.Width = 25;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Nom";
            this.Column2.Name = "Column2";
            this.Column2.Width = 200;
            // 
            // Column3
            // 
            this.Column3.HeaderText = "Nombre personnes";
            this.Column3.Name = "Column3";
            this.Column3.Width = 70;
            // 
            // Column4
            // 
            this.Column4.HeaderText = "Description courte";
            this.Column4.Name = "Column4";
            this.Column4.Width = 250;
            // 
            // Column5
            // 
            this.Column5.HeaderText = "Description longue";
            this.Column5.Name = "Column5";
            this.Column5.Width = 250;
            // 
            // Column6
            // 
            this.Column6.HeaderText = "Tarif";
            this.Column6.Name = "Column6";
            this.Column6.Width = 50;
            // 
            // btnAjout
            // 
            this.btnAjout.BackColor = System.Drawing.Color.White;
            this.btnAjout.FlatAppearance.BorderColor = System.Drawing.Color.LightGray;
            this.btnAjout.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAjout.Font = new System.Drawing.Font("Arial", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAjout.ForeColor = System.Drawing.Color.Black;
            this.btnAjout.Location = new System.Drawing.Point(9, 317);
            this.btnAjout.Margin = new System.Windows.Forms.Padding(2);
            this.btnAjout.Name = "btnAjout";
            this.btnAjout.Size = new System.Drawing.Size(118, 34);
            this.btnAjout.TabIndex = 5;
            this.btnAjout.Text = "Ajouter";
            this.btnAjout.UseVisualStyleBackColor = false;
            this.btnAjout.Click += new System.EventHandler(this.btnAjout_Click);
            // 
            // btnModif
            // 
            this.btnModif.BackColor = System.Drawing.Color.White;
            this.btnModif.FlatAppearance.BorderColor = System.Drawing.Color.LightGray;
            this.btnModif.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnModif.Font = new System.Drawing.Font("Arial", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnModif.ForeColor = System.Drawing.Color.Black;
            this.btnModif.Location = new System.Drawing.Point(131, 317);
            this.btnModif.Margin = new System.Windows.Forms.Padding(2);
            this.btnModif.Name = "btnModif";
            this.btnModif.Size = new System.Drawing.Size(118, 34);
            this.btnModif.TabIndex = 6;
            this.btnModif.Text = "Modifier";
            this.btnModif.UseVisualStyleBackColor = false;
            this.btnModif.Click += new System.EventHandler(this.btnModif_Click);
            // 
            // btnSuppr
            // 
            this.btnSuppr.BackColor = System.Drawing.Color.White;
            this.btnSuppr.FlatAppearance.BorderColor = System.Drawing.Color.LightGray;
            this.btnSuppr.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSuppr.Font = new System.Drawing.Font("Arial", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSuppr.ForeColor = System.Drawing.Color.Black;
            this.btnSuppr.Location = new System.Drawing.Point(9, 411);
            this.btnSuppr.Margin = new System.Windows.Forms.Padding(2);
            this.btnSuppr.Name = "btnSuppr";
            this.btnSuppr.Size = new System.Drawing.Size(233, 34);
            this.btnSuppr.TabIndex = 8;
            this.btnSuppr.Text = "Supprimer";
            this.btnSuppr.UseVisualStyleBackColor = false;
            this.btnSuppr.Click += new System.EventHandler(this.btnSuppr_Click);
            // 
            // txtNom
            // 
            this.txtNom.Location = new System.Drawing.Point(47, 46);
            this.txtNom.Margin = new System.Windows.Forms.Padding(2);
            this.txtNom.Name = "txtNom";
            this.txtNom.Size = new System.Drawing.Size(80, 20);
            this.txtNom.TabIndex = 0;
            // 
            // txtPrix
            // 
            this.txtPrix.Location = new System.Drawing.Point(170, 46);
            this.txtPrix.Margin = new System.Windows.Forms.Padding(2);
            this.txtPrix.Name = "txtPrix";
            this.txtPrix.Size = new System.Drawing.Size(80, 20);
            this.txtPrix.TabIndex = 1;
            // 
            // txtDescLong
            // 
            this.txtDescLong.Location = new System.Drawing.Point(11, 195);
            this.txtDescLong.Margin = new System.Windows.Forms.Padding(2);
            this.txtDescLong.Multiline = true;
            this.txtDescLong.Name = "txtDescLong";
            this.txtDescLong.Size = new System.Drawing.Size(232, 118);
            this.txtDescLong.TabIndex = 4;
            // 
            // txtDescCourt
            // 
            this.txtDescCourt.Location = new System.Drawing.Point(9, 110);
            this.txtDescCourt.Margin = new System.Windows.Forms.Padding(2);
            this.txtDescCourt.Multiline = true;
            this.txtDescCourt.Name = "txtDescCourt";
            this.txtDescCourt.Size = new System.Drawing.Size(234, 67);
            this.txtDescCourt.TabIndex = 3;
            // 
            // txtNbPers
            // 
            this.txtNbPers.Location = new System.Drawing.Point(131, 69);
            this.txtNbPers.Margin = new System.Windows.Forms.Padding(2);
            this.txtNbPers.Name = "txtNbPers";
            this.txtNbPers.Size = new System.Drawing.Size(119, 20);
            this.txtNbPers.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 48);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 31;
            this.label1.Text = "Nom :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 72);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(117, 13);
            this.label2.TabIndex = 32;
            this.label2.Text = "Nombre de personnes :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 179);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(101, 13);
            this.label3.TabIndex = 34;
            this.label3.Text = "Description longue :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, 94);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(99, 13);
            this.label4.TabIndex = 33;
            this.label4.Text = "Description courte :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(131, 48);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(34, 13);
            this.label5.TabIndex = 35;
            this.label5.Text = "Tarif :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(9, 366);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(194, 13);
            this.label6.TabIndex = 30;
            this.label6.Text = "ID de l\'élément a supprimer ou modifier :";
            // 
            // txtID
            // 
            this.txtID.Location = new System.Drawing.Point(11, 382);
            this.txtID.Margin = new System.Windows.Forms.Padding(2);
            this.txtID.Name = "txtID";
            this.txtID.Size = new System.Drawing.Size(234, 20);
            this.txtID.TabIndex = 7;
            // 
            // Fonctionnalite1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1240, 455);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtID);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtNbPers);
            this.Controls.Add(this.txtDescCourt);
            this.Controls.Add(this.txtDescLong);
            this.Controls.Add(this.txtPrix);
            this.Controls.Add(this.txtNom);
            this.Controls.Add(this.btnSuppr);
            this.Controls.Add(this.btnModif);
            this.Controls.Add(this.btnAjout);
            this.Controls.Add(this.grdAffType);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Fonctionnalite1";
            this.Text = "Fonctionnalite1";
            this.Load += new System.EventHandler(this.Fonctionnalite1_Load_1);
            ((System.ComponentModel.ISupportInitialize)(this.grdAffType)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView grdAffType;
        private System.Windows.Forms.Button btnAjout;
        private System.Windows.Forms.Button btnModif;
        private System.Windows.Forms.Button btnSuppr;
        private System.Windows.Forms.TextBox txtNom;
        private System.Windows.Forms.TextBox txtPrix;
        private System.Windows.Forms.TextBox txtDescLong;
        private System.Windows.Forms.TextBox txtDescCourt;
        private System.Windows.Forms.TextBox txtNbPers;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
    }
}