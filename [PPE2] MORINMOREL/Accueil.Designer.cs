﻿namespace _PPE2__MORINMOREL
{
    partial class Accueil
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fonctionnalitesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fonctionnalite2ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fonctionnalite3ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fonctionnalite4ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.quitterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fonctionnalitesToolStripMenuItem,
            this.fonctionnalite2ToolStripMenuItem,
            this.fonctionnalite3ToolStripMenuItem,
            this.fonctionnalite4ToolStripMenuItem,
            this.quitterToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1178, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fonctionnalitesToolStripMenuItem
            // 
            this.fonctionnalitesToolStripMenuItem.Name = "fonctionnalitesToolStripMenuItem";
            this.fonctionnalitesToolStripMenuItem.Size = new System.Drawing.Size(104, 20);
            this.fonctionnalitesToolStripMenuItem.Text = "Fonctionnalite 1";
            this.fonctionnalitesToolStripMenuItem.Click += new System.EventHandler(this.fonctionnalitesToolStripMenuItem_Click_1);
            // 
            // fonctionnalite2ToolStripMenuItem
            // 
            this.fonctionnalite2ToolStripMenuItem.Name = "fonctionnalite2ToolStripMenuItem";
            this.fonctionnalite2ToolStripMenuItem.Size = new System.Drawing.Size(150, 20);
            this.fonctionnalite2ToolStripMenuItem.Text = "Gestion des Mobilhomes";
            this.fonctionnalite2ToolStripMenuItem.Click += new System.EventHandler(this.fonctionnalite2ToolStripMenuItem_Click_1);
            // 
            // fonctionnalite3ToolStripMenuItem
            // 
            this.fonctionnalite3ToolStripMenuItem.Name = "fonctionnalite3ToolStripMenuItem";
            this.fonctionnalite3ToolStripMenuItem.Size = new System.Drawing.Size(104, 20);
            this.fonctionnalite3ToolStripMenuItem.Text = "Fonctionnalite 3";
            this.fonctionnalite3ToolStripMenuItem.Click += new System.EventHandler(this.fonctionnalite3ToolStripMenuItem_Click_1);
            // 
            // fonctionnalite4ToolStripMenuItem
            // 
            this.fonctionnalite4ToolStripMenuItem.Name = "fonctionnalite4ToolStripMenuItem";
            this.fonctionnalite4ToolStripMenuItem.Size = new System.Drawing.Size(99, 20);
            this.fonctionnalite4ToolStripMenuItem.Text = "Location Client";
            this.fonctionnalite4ToolStripMenuItem.Click += new System.EventHandler(this.fonctionnalite4ToolStripMenuItem_Click_1);
            // 
            // quitterToolStripMenuItem
            // 
            this.quitterToolStripMenuItem.Name = "quitterToolStripMenuItem";
            this.quitterToolStripMenuItem.Size = new System.Drawing.Size(56, 20);
            this.quitterToolStripMenuItem.Text = "Quitter";
            this.quitterToolStripMenuItem.Click += new System.EventHandler(this.quitterToolStripMenuItem_Click_1);
            // 
            // Accueil
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1178, 398);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Accueil";
            this.Text = "Accueil";
            this.Load += new System.EventHandler(this.Accueil_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStripMenuItem fonctionnalitesToolStripMenuItem;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fonctionnalite2ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fonctionnalite3ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fonctionnalite4ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem quitterToolStripMenuItem;
    }
}

