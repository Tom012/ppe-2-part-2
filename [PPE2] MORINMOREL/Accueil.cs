﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace _PPE2__MORINMOREL
{
    public partial class Accueil : Form
    {
        // Déclaration des formulaires enfants dans Form0
        Fonctionnalite1 form1;
        Fonctionnalite2 form2;
        Fonctionnalite3 form3;
        Fonctionnalite4 form4;
        public Accueil()
        {
            InitializeComponent();
        }

        private void Accueil_Load(object sender, EventArgs e)
        {
            //L'accueil est formulaire MDI Container des autres formulaires
            this.IsMdiContainer = true;
        }

        private void fonctionnalitesToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            // Avant de le créer, on teste que fonctionnalite1 est inexistant ou est fermé 
            if (form1 == null || form1.IsDisposed == true)
            {
                form1 = new Fonctionnalite1();
                form1.MdiParent = this; //  Fonctionnalite1 est formulaire enfant de l'accueil (this)
                form1.WindowState = FormWindowState.Maximized;
                form1.Show();
            }
            else
            {
                form1.Activate(); // Affichage au premier plan
            }
        }

        private void fonctionnalite2ToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            // Avant de le créer, on teste que fonctionnalite2 est inexistant ou est fermé 
            if (form2 == null || form2.IsDisposed == true)
            {
                form2 = new Fonctionnalite2();
                form2.MdiParent = this; //  Fonctionnalite2 est formulaire enfant de l'accueil (this)
                form2.WindowState = FormWindowState.Maximized;
                form2.Show();
            }
            else
            {
                form2.Activate(); // Affichage au premier plan
            }
        }

        private void fonctionnalite3ToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            // Avant de le créer, on teste que fonctionnalite3 est inexistant ou est fermé 
            if (form3 == null || form3.IsDisposed == true)
            {
                form3 = new Fonctionnalite3();
                form3.MdiParent = this; //  Fonctionnalite3 est formulaire enfant de l'accueil (this)
                form3.WindowState = FormWindowState.Maximized;
                form3.Show();
            }
            else
            {
                form3.Activate(); // Affichage au premier plan
            }
        }

        private void fonctionnalite4ToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            // Avant de le créer, on teste que fonctionnalite4 est inexistant ou est fermé 
            if (form4 == null || form4.IsDisposed == true)
            {
                form4 = new Fonctionnalite4();
                form4.MdiParent = this; //  Fonctionnalite4 est formulaire enfant de l'accueil (this)
                form4.WindowState = FormWindowState.Maximized;
                form4.Show();
            }
            else
            {
                form4.Activate(); // Affichage au premier plan
            }
        }

        private void quitterToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
