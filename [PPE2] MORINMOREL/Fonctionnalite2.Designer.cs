﻿namespace _PPE2__MORINMOREL
{
    partial class Fonctionnalite2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grdMobilhome = new System.Windows.Forms.DataGridView();
            this.txtNomAdd = new System.Windows.Forms.TextBox();
            this.txtNumempAdd = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnModif = new System.Windows.Forms.Button();
            this.btnDel = new System.Windows.Forms.Button();
            this.txtIdMod = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.CboTypeAdd = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtNomMod = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtNumempMod = new System.Windows.Forms.TextBox();
            this.CboTypeMod = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.grdMobilhome)).BeginInit();
            this.SuspendLayout();
            // 
            // grdMobilhome
            // 
            this.grdMobilhome.AllowUserToAddRows = false;
            this.grdMobilhome.AllowUserToDeleteRows = false;
            this.grdMobilhome.AllowUserToResizeColumns = false;
            this.grdMobilhome.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdMobilhome.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4});
            this.grdMobilhome.Location = new System.Drawing.Point(281, 9);
            this.grdMobilhome.MultiSelect = false;
            this.grdMobilhome.Name = "grdMobilhome";
            this.grdMobilhome.ReadOnly = true;
            this.grdMobilhome.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grdMobilhome.Size = new System.Drawing.Size(646, 296);
            this.grdMobilhome.TabIndex = 0;
            this.grdMobilhome.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdMobilhome_CellClick);
            // 
            // txtNomAdd
            // 
            this.txtNomAdd.Location = new System.Drawing.Point(12, 25);
            this.txtNomAdd.Name = "txtNomAdd";
            this.txtNomAdd.Size = new System.Drawing.Size(97, 20);
            this.txtNomAdd.TabIndex = 1;
            // 
            // txtNumempAdd
            // 
            this.txtNumempAdd.Location = new System.Drawing.Point(12, 64);
            this.txtNumempAdd.Name = "txtNumempAdd";
            this.txtNumempAdd.Size = new System.Drawing.Size(97, 20);
            this.txtNumempAdd.TabIndex = 6;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(104, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Nom du Mobilhome :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(124, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Numéro d\'emplacement :";
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(12, 90);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(121, 36);
            this.btnAdd.TabIndex = 9;
            this.btnAdd.Text = "AJOUTER";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click_1);
            // 
            // btnModif
            // 
            this.btnModif.Location = new System.Drawing.Point(12, 269);
            this.btnModif.Name = "btnModif";
            this.btnModif.Size = new System.Drawing.Size(121, 36);
            this.btnModif.TabIndex = 10;
            this.btnModif.Text = "MODIFIER";
            this.btnModif.UseVisualStyleBackColor = true;
            this.btnModif.Click += new System.EventHandler(this.btnModif_Click_1);
            // 
            // btnDel
            // 
            this.btnDel.Location = new System.Drawing.Point(139, 269);
            this.btnDel.Name = "btnDel";
            this.btnDel.Size = new System.Drawing.Size(136, 36);
            this.btnDel.TabIndex = 11;
            this.btnDel.Text = "SUPPRIMER";
            this.btnDel.UseVisualStyleBackColor = true;
            this.btnDel.Click += new System.EventHandler(this.btnDel_Click_1);
            // 
            // txtIdMod
            // 
            this.txtIdMod.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txtIdMod.Location = new System.Drawing.Point(12, 165);
            this.txtIdMod.Name = "txtIdMod";
            this.txtIdMod.Size = new System.Drawing.Size(97, 20);
            this.txtIdMod.TabIndex = 12;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 149);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(24, 13);
            this.label3.TabIndex = 13;
            this.label3.Text = "ID :";
            // 
            // CboTypeAdd
            // 
            this.CboTypeAdd.FormattingEnabled = true;
            this.CboTypeAdd.Location = new System.Drawing.Point(139, 25);
            this.CboTypeAdd.Name = "CboTypeAdd";
            this.CboTypeAdd.Size = new System.Drawing.Size(136, 21);
            this.CboTypeAdd.TabIndex = 14;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(136, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(106, 13);
            this.label4.TabIndex = 15;
            this.label4.Text = "Type de Mobilhome :";
            // 
            // txtNomMod
            // 
            this.txtNomMod.Location = new System.Drawing.Point(12, 204);
            this.txtNomMod.Name = "txtNomMod";
            this.txtNomMod.Size = new System.Drawing.Size(97, 20);
            this.txtNomMod.TabIndex = 16;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(9, 188);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(104, 13);
            this.label5.TabIndex = 17;
            this.label5.Text = "Nom du Mobilhome :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(9, 227);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(124, 13);
            this.label6.TabIndex = 18;
            this.label6.Text = "Numéro d\'emplacement :";
            // 
            // txtNumempMod
            // 
            this.txtNumempMod.Location = new System.Drawing.Point(12, 243);
            this.txtNumempMod.Name = "txtNumempMod";
            this.txtNumempMod.Size = new System.Drawing.Size(97, 20);
            this.txtNumempMod.TabIndex = 19;
            // 
            // CboTypeMod
            // 
            this.CboTypeMod.FormattingEnabled = true;
            this.CboTypeMod.Location = new System.Drawing.Point(139, 164);
            this.CboTypeMod.Name = "CboTypeMod";
            this.CboTypeMod.Size = new System.Drawing.Size(136, 21);
            this.CboTypeMod.TabIndex = 20;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(136, 148);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(106, 13);
            this.label7.TabIndex = 21;
            this.label7.Text = "Type de Mobilhome :";
            // 
            // Column1
            // 
            this.Column1.HeaderText = "ID";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 50;
            // 
            // Column2
            // 
            this.Column2.FillWeight = 10.20409F;
            this.Column2.HeaderText = "Nom";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Width = 150;
            // 
            // Column3
            // 
            this.Column3.FillWeight = 279.5918F;
            this.Column3.HeaderText = "Numero d\'emplacement";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Width = 150;
            // 
            // Column4
            // 
            this.Column4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column4.FillWeight = 10.20409F;
            this.Column4.HeaderText = "Type";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            // 
            // Fonctionnalite2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(939, 315);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.CboTypeMod);
            this.Controls.Add(this.txtNumempMod);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtNomMod);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.CboTypeAdd);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtIdMod);
            this.Controls.Add(this.btnDel);
            this.Controls.Add(this.btnModif);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtNumempAdd);
            this.Controls.Add(this.txtNomAdd);
            this.Controls.Add(this.grdMobilhome);
            this.Name = "Fonctionnalite2";
            this.Text = "Mobilhome";
            this.Load += new System.EventHandler(this.Fonctionnalite2_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grdMobilhome)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView grdMobilhome;
        private System.Windows.Forms.TextBox txtNomAdd;
        private System.Windows.Forms.TextBox txtNumempAdd;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnModif;
        private System.Windows.Forms.Button btnDel;
        private System.Windows.Forms.TextBox txtIdMod;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox CboTypeAdd;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtNomMod;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtNumempMod;
        private System.Windows.Forms.ComboBox CboTypeMod;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
    }
}